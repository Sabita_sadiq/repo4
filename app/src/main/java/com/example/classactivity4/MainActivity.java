package com.example.classactivity4;

import android.os.Bundle;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import java.util.regex.Matcher;
import java.util.regex.Pattern;




public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final EditText your_firstname = (EditText) findViewById(R.id.editText1);
        final EditText your_lastname = (EditText) findViewById(R.id.editText2);
        final EditText your_section = (EditText) findViewById(R.id.editText3);

        Button check = (Button) findViewById(R.id.button_submit);
        check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String firstname = your_firstname.getText().toString();
                String lastname = your_lastname.getText().toString();
                String section = your_section.getText().toString();
                
                if (TextUtils.isEmpty(firstname)) {
                    your_firstname.setError("Enter Your First Name");
                    your_firstname.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty(lastname)) {
                    your_lastname.setError("Enter Your Last Name");
                    your_lastname.requestFocus();
                    return;
                }
                if (TextUtils.isEmpty(section)) {
                    your_section.setError("Enter Your Section");
                    your_section.requestFocus();
                    return;


                }
                Intent intent = new Intent(getApplicationContext(), SeconndActivity.class);
                intent.putExtra("First_name", firstname);
                intent.putExtra("Last_name",lastname);
                intent.putExtra("Section",section);

                startActivity(intent);
            }

        });


    }


}