package com.example.classactivity4;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;



public class SeconndActivity extends AppCompatActivity {
    TextView text,text1,text2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        text = (TextView)findViewById(R.id.text);
        text1 = (TextView)findViewById(R.id.text1);
        text2 = (TextView)findViewById(R.id.text2);

        Intent intent = getIntent();
        String name = intent.getStringExtra("First_name");
        String subject = intent.getStringExtra("Last_name");
        String message = intent.getStringExtra("Section");
        text.setText(name);
        text1.setText(subject);
        text2.setText(message);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

}